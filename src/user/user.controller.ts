import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtGuard } from 'src/guard';
import { GetUser } from 'src/decorator';
import { UserService } from './user.service';
import { DefaultResult } from 'src/shared/dto';
import { GetUsersListRequest, UpdateProfileRequest } from 'src/dto/requests';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @UseGuards(JwtGuard)
  @HttpCode(HttpStatus.OK)
  @Get('me')
  getMyProfile(
    // This method gets the complete payload of the user
    // @GetUser() user: User,
    @GetUser('id') id: number,
  ): Promise<DefaultResult> {
    return this.userService.getMyProfile(id);
  }

  @UseGuards(JwtGuard)
  @HttpCode(HttpStatus.OK)
  @Patch('update/:id')
  updateUserProfile(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateProfileRequest,
  ): Promise<DefaultResult> {
    return this.userService.updateUserprofile(id, dto);
  }

  // TODO: only admin can see this list
  @UseGuards(JwtGuard)
  @HttpCode(HttpStatus.OK)
  @Get('list')
  getUsersList(@Query() dto: GetUsersListRequest): Promise<DefaultResult> {
    return this.userService.getUsersList(dto);
  }
}
