// Nest Modules
import { Injectable, NotFoundException } from '@nestjs/common';

// DTOs and Utils
import { GetUsersListRequest, UpdateProfileRequest } from 'src/dto/requests';
import { UserProfileResponse } from '../dto/responses';
import { DefaultResult } from 'src/shared/dto';
import { RESPONSE_MESSAGES } from 'src/utils';

// Dependecies and Services
import { UserRepository } from './user.repository';
import { User } from '@prisma/client';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  async getMyProfile(id: number): Promise<DefaultResult<UserProfileResponse>> {
    // find user profile
    const user = await this.userRepository.findUserById(id);
    if (!user) throw new NotFoundException('User NotFound');

    // create response dto
    const userProfile = this.userProfileMapper(user);

    const response: DefaultResult<UserProfileResponse> = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: userProfile,
    };

    return response;
  }

  async updateUserprofile(
    id: number,
    data: UpdateProfileRequest,
  ): Promise<DefaultResult<UserProfileResponse>> {
    // if email changed, user should verify email again
    // TODO: implement it

    // update user in database
    const user = await this.userRepository.updateUserProfileById(id, data);

    // create response dto
    const userProfile = this.userProfileMapper(user);

    const response: DefaultResult<UserProfileResponse> = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: userProfile,
    };

    return response;
  }

  async getUsersList(
    data: GetUsersListRequest,
  ): Promise<DefaultResult<UserProfileResponse>> {
    // get users from db
    const users = await this.userRepository.findUsersByQuery(data);

    // create response dto
    const usersProfile = users.map(user => {
      return this.userProfileMapper(user);
    });

    const response: DefaultResult<UserProfileResponse> = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: usersProfile,
    };

    return response;
  }

  private userProfileMapper(user: User): UserProfileResponse {
    const userProfile: UserProfileResponse = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    };

    return userProfile;
  }
}
