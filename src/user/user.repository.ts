import { GetUsersListRequest, UpdateProfileRequest } from 'src/dto/requests';
import { BadGatewayException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { User } from '@prisma/client';

@Injectable()
export class UserRepository {
  constructor(private prisma: PrismaService) {}

  async createUser(email: string, password: string): Promise<User> {
    try {
      return await this.prisma.user.create({
        data: {
          email,
          password,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async findUserByEmail(email: string): Promise<User | null> {
    try {
      return await this.prisma.user.findUnique({
        where: {
          email: email,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async findUserById(userId: number): Promise<User | null> {
    try {
      return await this.prisma.user.findUnique({
        where: {
          id: userId,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async verifyUserToken(userId: number): Promise<User> {
    try {
      return await this.prisma.user.update({
        where: {
          id: userId,
        },
        data: {
          emailVerified: true,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async updateUserPasswordById(
    userId: number,
    password: string,
  ): Promise<User> {
    try {
      return await this.prisma.user.update({
        where: {
          id: userId,
        },
        data: {
          password,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async updateUserProfileById(
    userId: number,
    data: UpdateProfileRequest,
  ): Promise<User> {
    try {
      return await this.prisma.user.update({
        where: {
          id: userId,
        },
        data: {
          ...data,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async findUsersByQuery(data: GetUsersListRequest): Promise<User[]> {
    try {
      return await this.prisma.user.findMany({
        where: {
          ...data,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async updateUserTfaStatus(userId: number, status: boolean): Promise<User> {
    try {
      return await this.prisma.user.update({
        where: {
          id: userId,
        },
        data: {
          isTfaEnabled: status,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}
