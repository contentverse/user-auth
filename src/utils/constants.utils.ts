export enum RESPONSE_MESSAGES {
  OK = 'OK',
  CREATED = 'CREATED',
  UPDATED = 'UPDATED',
  DELETED = 'DELETED',
}

export enum TOKEN_TYPES {
  SIGNIN = 'signin',
  FORGOT_PASSWORD = 'forgot-password',
}
