import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { AuthRepository } from './auth.repository';
import { UserRepository } from 'src/user/user.repository';
@Module({
  imports: [
    JwtModule.register({
      // global: true,
      // secret: JwtConstants.SECRET,
      // signOptions: { expiresIn: JwtConstants.ACCESS_TOKEN_EXPIREIN },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, AuthRepository, JwtStrategy, UserRepository],
})
export class AuthModule {}
