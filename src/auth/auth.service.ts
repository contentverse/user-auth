// Nest Modules
import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

// DTOs and Utils
import {
  GetNewTokenRequest,
  PasswordValidation,
  SignupRequest,
} from '../dto/requests';
import {
  RESPONSE_MESSAGES,
  TOKEN_TYPES,
  generateRandomString,
} from 'src/utils';
import { Get2Fa, SigninResponse, generateBackupCodes } from '../dto/responses';
import { DefaultResult, JwtTokenPayload } from 'src/shared/dto';

// Dependecies and Services
import * as argon from 'argon2';
import * as crypto from 'crypto';
import { User } from '@prisma/client';
import * as speakeasy from 'speakeasy';
import { AuthRepository } from './auth.repository';
import { UserRepository } from 'src/user/user.repository';
import { LoggerService } from 'src/logger/logger.service';

@Injectable()
export class AuthService {
  constructor(
    private logger: LoggerService,
    private config: ConfigService,
    private jwt: JwtService,
    private authRepository: AuthRepository,
    private userRepository: UserRepository,
  ) {}

  async signup(data: SignupRequest): Promise<DefaultResult> {
    this.logger.log({
      message: 'user wants to signup',
      function: 'signup',
      email: data.email,
    });

    // check exist user email (duplicate)
    const checkExistUser = await this.userRepository.findUserByEmail(
      data.email,
    );

    if (checkExistUser) throw new ConflictException('Credentials taken!');

    // hash the user password
    this.logger.log({
      message: 'hashing user password',
      function: 'signin',
      email: data.email,
    });

    const hashedPassword = await argon.hash(data.password);

    // save user in db
    this.logger.log({
      message: 'creating user account',
      function: 'signin',
      email: data.email,
    });

    const user = await this.userRepository.createUser(
      data.email,
      hashedPassword,
    );

    this.logger.log({
      message: 'user account created',
      function: 'signin',
      email: data.email,
      userId: user.id,
    });

    // create token for verify email
    const token = generateRandomString(32);

    const expireAt = new Date();
    expireAt.setDate(expireAt.getDate() + 1);

    // save token in db
    await this.authRepository.createToken(
      token,
      TOKEN_TYPES.SIGNIN,
      user.id,
      expireAt,
    );

    // send verify email notification to user
    this.logger.log({
      message: 'sending verify email notification to user',
      function: 'signin',
      email: data.email,
      userId: user.id,
    });
    // TODO: send notification to user

    this.logger.log({
      message: 'user signed up',
      function: 'signin',
      email: data.email,
      userId: user.id,
    });

    // create response dto
    const response: DefaultResult = {
      success: true,
      message: RESPONSE_MESSAGES.CREATED,
      data: {
        message: 'The verification code has been sent to your email',
      },
    };

    return response;
  }

  async validateSignup(token: string): Promise<DefaultResult> {
    this.logger.log({
      message: 'user wants to verify their account',
      function: 'validateSignup',
    });

    // find token
    const findToken = await this.authRepository.findToken(
      token,
      TOKEN_TYPES.SIGNIN,
    );

    if (!findToken) throw new ForbiddenException('Credentials incorrect');

    this.logger.log({
      message: 'verifing user account',
      function: 'validateSignup',
      userId: findToken.userId,
    });

    // validate user
    await this.userRepository.verifyUserToken(findToken.userId);

    // modify token to is used
    await this.authRepository.changeToIsUsedToken(findToken.id);

    this.logger.log({
      message: 'user account verified',
      function: 'validateSignup',
      userId: findToken.userId,
    });

    // create response dto
    const response: DefaultResult = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: {
        message: 'Your email validated successfully',
      },
    };

    return response;
  }

  async signin(data: SignupRequest): Promise<DefaultResult<SigninResponse>> {
    this.logger.log({
      message: 'user wants to login',
      function: 'signin',
      email: data.email,
    });

    // find user
    const user = await this.userRepository.findUserByEmail(data.email);

    if (user && user.emailVerified == false)
      throw new ForbiddenException('Please verify your email');

    if (!user) throw new ForbiddenException('Credentials incorrect');

    this.logger.log({
      message: 'comapring user password',
      function: 'signin',
      email: data.email,
      userId: user.id,
    });

    // check user password
    await this.compareUserPassword(user, data.password);

    // TODO: add google login with passport

    // generate JWT tokens
    const tokens = await this.signTokens(user.id, user.email);

    this.logger.log({
      message: 'user logged in',
      function: 'signin',
      email: data.email,
      userId: user.id,
    });

    // create response dto
    const response: DefaultResult<SigninResponse> = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: tokens,
    };

    return response;
  }

  async refreshToken(
    data: GetNewTokenRequest,
  ): Promise<DefaultResult<SigninResponse>> {
    this.logger.log({
      message: 'user wants to refresh their token',
      function: 'refreshToken',
    });

    // verify token
    const verify = await this.verifyRefreshToken(data.refresh_token);

    // generate JWT tokens
    const tokens = await this.signTokens(verify.id, verify.email);

    this.logger.log({
      message: 'user refresh their token',
      function: 'refreshToken',
      userId: verify.id,
    });

    // create response dto
    const response: DefaultResult<SigninResponse> = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: tokens,
    };

    return response;
  }

  async forgotPassword(email: string): Promise<DefaultResult> {
    this.logger.log({
      message: 'user forgot their password',
      function: 'forgotPassword',
      email,
    });

    // find user
    const user = await this.userRepository.findUserByEmail(email);

    if (!user) throw new NotFoundException('Credentials incorrect');

    this.logger.log({
      message: 'generation forget password token',
      function: 'forgotPassword',
      email,
      userId: user.id,
    });

    // create token for verify forgot password
    const token = generateRandomString(32);

    const expireAt = new Date();
    expireAt.setDate(expireAt.getMinutes() + 5);

    // save token in db
    await this.authRepository.createToken(
      token,
      TOKEN_TYPES.FORGOT_PASSWORD,
      user.id,
      expireAt,
    );

    // send forgot password email notification to user
    this.logger.log({
      message: 'sending forgot password email notification to user',
      function: 'forgotPassword',
      email,
      userId: user.id,
    });
    // TODO: send notification to user

    this.logger.log({
      message: 'forgot password email notification sent to user',
      function: 'forgotPassword',
      email,
      userId: user.id,
    });

    // create response dto
    const response: DefaultResult = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: {
        message: 'The recovery code has been sent to your email',
      },
    };

    return response;
  }

  async changePassword(
    token: string,
    password: string,
  ): Promise<DefaultResult> {
    this.logger.log({
      message: 'user wants to change their password',
      function: 'changePassword',
    });

    // find token
    const findToken = await this.authRepository.findToken(
      token,
      TOKEN_TYPES.FORGOT_PASSWORD,
    );

    if (!findToken) throw new ForbiddenException('Credentials incorrect');

    // hash new password
    this.logger.log({
      message: 'hashing user password',
      function: 'changePassword',
      userId: findToken.userId,
    });

    const hashedPassword = await argon.hash(password);

    // update user password in db
    this.logger.log({
      message: 'updating user password in database',
      function: 'changePassword',
      userId: findToken.userId,
    });

    await this.userRepository.updateUserPasswordById(
      findToken.userId,
      hashedPassword,
    );

    // TODO: user sessions and tokens should expire

    // modify token to is used
    await this.authRepository.changeToIsUsedToken(findToken.id);

    this.logger.log({
      message: 'user password changed',
      function: 'changePassword',
      userId: findToken.userId,
    });

    // create response dto
    const response: DefaultResult = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: {
        message: 'Your password changed successfully',
      },
    };

    return response;
  }

  async enableTfa(
    userId: number,
    data: PasswordValidation,
  ): Promise<DefaultResult> {
    this.logger.log({
      message: 'user wants to enable 2FA',
      function: 'enableTfa',
      userId,
    });

    // find user
    const user = await this.userRepository.findUserById(userId);

    if (!user) throw new NotFoundException('User NotFound');

    // compare password
    this.logger.log({
      message: 'comparing user password',
      function: 'enableTfa',
      userId,
    });

    const pwMatches = await argon.verify(user.password, data.password);

    if (!pwMatches) throw new ForbiddenException('Credentials incorrect');

    // check user 2FA status
    if (user.isTfaEnabled == true)
      throw new ForbiddenException('Two factor authentication already enabled');

    // generate 2FA for user
    this.logger.log({
      message: 'generating 2FA for user',
      function: 'enableTfa',
      userId,
    });

    const tfaSecret = this.generateTwoFactorAuth(user.email);

    // save 2FA secret in db
    this.logger.log({
      message: 'saving 2FA hash in user account',
      function: 'enableTfa',
      userId,
    });

    await this.authRepository.createTfaForUser(userId, tfaSecret.hash);

    // update user 2FA status
    await this.userRepository.updateUserTfaStatus(userId, true);

    this.logger.log({
      message: 'user 2FA enabled',
      function: 'enableTfa',
      userId,
    });

    // create response dto
    const response: DefaultResult = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: {
        url: tfaSecret.url,
      },
    };

    return response;
  }

  async getBackupCodes(
    userId: number,
    data: PasswordValidation,
  ): Promise<DefaultResult> {
    // find user
    const user = await this.userRepository.findUserById(userId);

    if (!user) throw new NotFoundException('User NotFound');

    // check user password
    await this.compareUserPassword(user, data.password);

    // delete old backup codes, if their exist
    const userBackupCodes = await this.authRepository.findBackupCodesByUserId(
      userId,
    );

    if (userBackupCodes.length > 0)
      await this.authRepository.removeUserBackupCodes(userId);

    // Generate 10 backup codes, each 10 digits long
    const { backupCodes, backupCodesObject } = this.generateBackupCodes(userId);

    // save backup codes in db
    await this.authRepository.createBackupCodesForUser(backupCodesObject);

    // create response dto
    const response: DefaultResult = {
      success: true,
      message: RESPONSE_MESSAGES.OK,
      data: backupCodes,
    };

    return response;
  }

  private async compareUserPassword(user: User, password: string) {
    let pwMatches: boolean = false;

    // check for backup code
    const backupCode = await this.authRepository.getOneBackupCode(
      user.id,
      password,
    );

    if (backupCode) {
      this.logger.debug({
        message: 'comparing password for user with backup code',
        function: 'compareUserPassword',
        userId: user.id,
      });

      // change to is used backup code
      await this.authRepository.changeToIsUsedBackupCode(backupCode.id);

      pwMatches = true;
    } else if (user.isTfaEnabled) {
      this.logger.debug({
        message: 'comparing password for user with 2FA',
        function: 'compareUserPassword',
        userId: user.id,
      });

      const tfa = await this.authRepository.getTfaByUserId(user.id);

      pwMatches = this.isTfaValid(tfa.secret, password);
    } else {
      this.logger.debug({
        message: 'comparing password for user',
        function: 'compareUserPassword',
        userId: user.id,
      });

      pwMatches = await argon.verify(user.password, password);
    }

    this.logger.log({
      message: `password comparing result is ${pwMatches}`,
      function: 'compareUserPassword',
      userId: user.id,
    });

    if (!pwMatches) throw new ForbiddenException('Credentials incorrect');

    return true;
  }

  // JWT
  private async signTokens(id: number, email: string): Promise<SigninResponse> {
    this.logger.log({
      message: 'generating JWT tokens for user',
      function: 'signTokens',
      email: email,
      userId: id,
    });

    // access token
    const accessTokenPayload: JwtTokenPayload = {
      id,
      email,
      tokenType: 'access-token',
    };

    const accessToken = await this.jwt.signAsync(accessTokenPayload, {
      expiresIn: this.config.get('ACCESS_TOKEN_EXPIREIN'),
      secret: this.config.get('ACCESS_TOKEN_SECRET'),
    });

    this.logger.debug({
      message: 'access token generated',
      function: 'signTokens',
      email: email,
      userId: id,
    });

    // refresh token
    const refreshTokenPayload: JwtTokenPayload = {
      id,
      email,
      tokenType: 'refresh-token',
    };

    const refreshToken = await this.jwt.signAsync(refreshTokenPayload, {
      expiresIn: this.config.get('REFRESH_TOKEN_EXPIREIN'),
      secret: this.config.get('REFRESH_TOKEN_SECRET'),
    });

    this.logger.debug({
      message: 'refresh token generated',
      function: 'signTokens',
      email: email,
      userId: id,
    });

    // create response dto
    const response: SigninResponse = {
      access_token: accessToken,
      refresh_token: refreshToken,
    };

    return response;
  }

  private async verifyRefreshToken(token: string): Promise<JwtTokenPayload> {
    try {
      this.logger.debug({
        message: 'varifing user token',
        function: 'verifyRefreshToken',
      });

      return await this.jwt.verifyAsync(token, {
        secret: this.config.get('REFRESH_TOKEN_SECRET'),
      });
    } catch (error) {
      throw new ForbiddenException('Credentials incorrect');
    }
  }

  // SpeakEasy
  private generateTwoFactorAuth(email: string): Get2Fa {
    this.logger.debug({
      message: 'generating 2FA with speakeasy',
      function: 'generateTwoFactorAuth',
      email,
    });

    const secret = speakeasy.generateSecret({
      length: 32,
      issuer: 'ContentVerse',
      name: `ContentVerse:${email}`,
    });

    const hash = this.encryptSecret(secret.base32);

    this.logger.debug({
      message: 'speakeasy generated 2FA',
      function: 'generateTwoFactorAuth',
      email,
    });

    return {
      url: secret.otpauth_url,
      hash,
    };
  }

  private isTfaValid(hash: string, token: string) {
    const secret = this.decryptSecret(hash);

    this.logger.debug({
      message: 'verifing 2FA with speakeasy',
      function: 'isTfaValid',
    });

    return speakeasy.totp.verify({
      secret,
      encoding: 'base32',
      token,
    });
  }

  // Crypto
  private encryptSecret(token: string): string {
    this.logger.debug({
      message: 'crypto encryption begin',
      function: 'encryptSecret',
    });

    const encryptionKeyHex = this.config.get('ENCRYPT_KEY');
    const encryptionKey = Buffer.from(encryptionKeyHex, 'hex');

    const iv = crypto.randomBytes(16);

    const cipher = crypto.createCipheriv(
      'aes-256-cbc',
      Buffer.from(encryptionKey),
      iv,
    );

    this.logger.debug({
      message: 'crypto encryption created',
      function: 'encryptSecret',
    });

    let encryptedSecret = cipher.update(token, 'utf8', 'hex');
    encryptedSecret += cipher.final('hex');

    const ivHex = iv.toString('hex');
    encryptedSecret = ivHex + encryptedSecret;

    this.logger.debug({
      message: 'crypto encryption done',
      function: 'encryptSecret',
    });

    return encryptedSecret;
  }

  private decryptSecret(hash: string): string {
    this.logger.debug({
      message: 'crypto decryption begin',
      function: 'decryptSecret',
    });

    const encryptionKeyHex = this.config.get('ENCRYPT_KEY');
    const encryptionKey = Buffer.from(encryptionKeyHex, 'hex');

    const ivHex = hash.substring(0, 32);
    const iv = Buffer.from(ivHex, 'hex');
    const encryptedText = hash.substring(32);

    const decipher = crypto.createDecipheriv('aes-256-cbc', encryptionKey, iv);

    this.logger.debug({
      message: 'crypto decryption created',
      function: 'decryptSecret',
    });

    let decryptedSecret = decipher.update(encryptedText, 'hex', 'utf8');
    decryptedSecret += decipher.final('utf8');

    this.logger.debug({
      message: 'crypto decryption done',
      function: 'decryptSecret',
    });

    return decryptedSecret;
  }

  private generateBackupCodes(userId: number): generateBackupCodes {
    const backupCodes = Array.from({ length: 10 }, () =>
      crypto.randomInt(1000000000, 9999999999),
    );

    const backupCodesObject = backupCodes.map(code => ({
      value: code.toString(),
      isUsed: false,
      userId,
    }));

    return {
      backupCodes,
      backupCodesObject,
    };
  }
}
