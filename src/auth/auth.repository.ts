import { BadGatewayException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { BackupCode, Tfa, Token } from '@prisma/client';
import { BackupCodes } from 'src/dto/responses';

@Injectable()
export class AuthRepository {
  constructor(private prisma: PrismaService) {}

  // Token
  async createToken(
    token: string,
    type: string,
    userId: number,
    expiredAt: Date,
  ): Promise<Token> {
    try {
      return await this.prisma.token.create({
        data: {
          token,
          type,
          userId,
          expiredAt,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async findToken(token: string, type: string): Promise<Token | null> {
    try {
      return await this.prisma.token.findFirst({
        where: {
          token,
          type,
          isUsed: false,
          expiredAt: {
            gt: new Date(),
          },
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async changeToIsUsedToken(tokenId: number): Promise<Token> {
    try {
      return await this.prisma.token.update({
        where: {
          id: tokenId,
        },
        data: {
          isUsed: true,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  // TFA
  async createTfaForUser(userId: number, secret: string): Promise<Tfa> {
    try {
      return await this.prisma.tfa.create({
        data: {
          userId,
          secret,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async getTfaByUserId(userId: number): Promise<Tfa | null> {
    try {
      return await this.prisma.tfa.findUnique({
        where: {
          userId,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  // Backup codes
  async createBackupCodesForUser(backupCodes: BackupCodes[]) {
    try {
      return await this.prisma.backupCode.createMany({
        data: backupCodes.map(item => ({
          value: item.value,
          isUsed: item.isUsed,
          userId: item.userId,
        })),
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async getOneBackupCode(
    userId: number,
    code: string,
  ): Promise<BackupCode | null> {
    try {
      return await this.prisma.backupCode.findFirst({
        where: {
          userId,
          value: code,
          isUsed: false,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async findBackupCodesByUserId(userId: number): Promise<BackupCode[]> {
    try {
      return await this.prisma.backupCode.findMany({
        where: {
          userId,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async removeUserBackupCodes(userId: number) {
    try {
      return await this.prisma.backupCode.deleteMany({
        where: {
          userId,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async changeToIsUsedBackupCode(id: number): Promise<BackupCode> {
    try {
      return await this.prisma.backupCode.update({
        where: {
          id,
        },
        data: {
          isUsed: true,
        },
      });
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}
