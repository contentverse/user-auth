import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  GetNewTokenRequest,
  PasswordValidation,
  SignupRequest,
} from '../dto/requests';
import { JwtGuard } from 'src/guard';
import { GetUser } from 'src/decorator';
import { AuthService } from './auth.service';
import { DefaultResult } from 'src/shared/dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signup')
  signup(@Body() dto: SignupRequest): Promise<DefaultResult> {
    return this.authService.signup(dto);
  }

  @HttpCode(HttpStatus.OK)
  @Get('validate-signup/:token')
  validateSignup(@Param('token') token: string): Promise<DefaultResult> {
    return this.authService.validateSignup(token);
  }

  @HttpCode(HttpStatus.OK)
  @Post('signin')
  async signin(@Body() dto: SignupRequest): Promise<DefaultResult> {
    return await this.authService.signin(dto);
  }

  @HttpCode(HttpStatus.OK)
  @Post('refresh-token')
  async refreshToken(@Body() dto: GetNewTokenRequest): Promise<DefaultResult> {
    return await this.authService.refreshToken(dto);
  }

  @UseGuards(JwtGuard)
  @HttpCode(HttpStatus.OK)
  @Post('tfa')
  async enableTfa(
    @GetUser('id') id: number,
    @Body() password: PasswordValidation,
  ): Promise<DefaultResult> {
    return await this.authService.enableTfa(id, password);
  }

  @UseGuards(JwtGuard)
  @HttpCode(HttpStatus.OK)
  @Post('backup-codes')
  async getBackupCodes(
    @GetUser('id') id: number,
    @Body() password: PasswordValidation,
  ): Promise<DefaultResult> {
    return await this.authService.getBackupCodes(id, password);
  }
}
