import { RESPONSE_MESSAGES } from 'src/utils';

export interface DefaultResult<T = object> {
  success: boolean;
  message: RESPONSE_MESSAGES;
  data: T | T[];
}
