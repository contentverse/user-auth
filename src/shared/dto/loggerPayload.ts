export interface LoggerPayload {
  message: string;
  function: string;
  userId?: number;
  email?: string;
}
