export class JwtTokenPayload {
  id: number;
  email: string;
  tokenType: string;
}
