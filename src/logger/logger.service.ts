import TransportElastic from 'elasticsearch-transport';
import { Injectable, Logger } from '@nestjs/common';
import { LoggerPayload } from 'src/shared/dto';
import { ConfigService } from '@nestjs/config';
import * as winston from 'winston';

@Injectable()
export class LoggerService extends Logger {
  private logger: winston.Logger;

  constructor(private config: ConfigService) {
    super();
    this.configureLogger();
  }

  private configureLogger() {
    winston.addColors({
      crit: 'underline magenta', // used for tools errors (mongo, rabbitmq)
      error: 'bold red', // only used for http errors (error handling)
      warning: 'bold yellow', // used in middlewares
      notice: 'bold blue', // used for http requests (streaming with morgan)
      info: 'green', // used for main functions
      debug: 'white', // extra informations and sub functions
    });

    const consleFormat = winston.format.combine(
      winston.format.splat(),
      winston.format.colorize({ all: true }),
      winston.format.label({ label: '[LOGGER]' }),
      winston.format.timestamp({ format: 'isoDateTime' }),
      winston.format.printf(
        item =>
          `${item.label} ${item.timestamp} ${item.level} : ${item.message}`,
      ),
    );

    const consoleOptions = {
      level: this.config.get('LOG_LEVEL'),
      format: consleFormat,
      handleExceptions: true,
      json: false,
    };

    const elasticOptions = {
      silent: false,
      elasticClient: {
        name: this.config.get('ELASTICSEARCH_INDEX'),
        node: this.config.get('ELASTICSEARCH_URL'),
        maxRetries: +this.config.get('ELASTICSEARCH_MAX_RETRIES'),
        setTimeout: +this.config.get('ELASTICSEARCH_TIMEOUT'),
        auth: {
          username: this.config.get('ELASTICSEARCH_USERNAME'),
          password: this.config.get('ELASTICSEARCH_PASSWORD'),
        },
      },
    };

    this.logger = winston.createLogger({
      levels: winston.config.syslog.levels,
      exitOnError: false,
      transports: [
        new winston.transports.Console(consoleOptions),
        new TransportElastic(elasticOptions),
      ],
    });

    this.logger.info('logger service is ready to use', {
      node: this.config.get('ELASTICSEARCH_URL'),
      maxRetries: +this.config.get('ELASTICSEARCH_MAX_RETRIES'),
      name: 'test_nameee',
    });
  }

  log(data: LoggerPayload) {
    const { message, ...variables } = data;
    this.logger.info(message, variables);
  }

  notice(data: LoggerPayload) {
    const { message, ...variables } = data;
    this.logger.notice(message, variables);
  }

  error(data: LoggerPayload) {
    const { message, ...variables } = data;
    this.logger.error(message, variables);
  }

  warning(data: LoggerPayload) {
    const { message, ...variables } = data;
    this.logger.warn(message, variables);
  }

  debug(data: LoggerPayload) {
    const { message, ...variables } = data;
    this.logger.debug(message, variables);
  }

  critical(data: LoggerPayload) {
    const { message, ...variables } = data;
    this.logger.crit(message, variables);
  }
}
