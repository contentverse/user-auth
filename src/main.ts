import { LoggerService } from './logger/logger.service';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  // port
  const configService = new ConfigService();
  const port = configService.get('PORT');

  await app.startAllMicroservices();
  await app.listen(port);

  LoggerService.log(`Application is runnig on port ${port}`);
}

bootstrap();
