export interface Get2Fa {
  url: string;
  hash: string;
}
