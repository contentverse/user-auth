export interface generateBackupCodes {
  backupCodes: number[];
  backupCodesObject: BackupCodes[];
}

export interface BackupCodes {
  value: string;
  isUsed: boolean;
  userId: number;
}
