export interface SigninResponse {
  access_token: string;
  refresh_token: string;
}
