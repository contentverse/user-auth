import { IsEmail, IsNotEmpty } from 'class-validator';

export class EmailValidation {
  @IsEmail()
  @IsNotEmpty()
  email: string;
}
