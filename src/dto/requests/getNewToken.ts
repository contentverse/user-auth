import { IsNotEmpty, IsString } from 'class-validator';

export class GetNewTokenRequest {
  @IsString()
  @IsNotEmpty()
  refresh_token: string;
}
