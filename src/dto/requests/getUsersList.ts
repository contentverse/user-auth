import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class GetUsersListRequest {
  @IsBoolean()
  @IsOptional()
  emailVerified?: boolean;

  @IsString()
  @IsOptional()
  firstName?: string;

  @IsString()
  @IsOptional()
  lastName?: string;
}
