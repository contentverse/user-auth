import { IsNotEmpty, IsString } from 'class-validator';

export class PasswordValidation {
  @IsString()
  @IsNotEmpty()
  password: string;
}
