import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SignupRequest {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
