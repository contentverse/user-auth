-- AlterTable
ALTER TABLE "users" ADD COLUMN     "isTfaEnabled" BOOLEAN NOT NULL DEFAULT false;

-- CreateTable
CREATE TABLE "tfaSecrets" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "secret" TEXT NOT NULL,
    "userId" INTEGER NOT NULL,

    CONSTRAINT "tfaSecrets_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "backupCodes" (
    "id" SERIAL NOT NULL,
    "value" TEXT NOT NULL,
    "isUsed" BOOLEAN NOT NULL,
    "userId" INTEGER NOT NULL,

    CONSTRAINT "backupCodes_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "tfaSecrets_userId_key" ON "tfaSecrets"("userId");

-- AddForeignKey
ALTER TABLE "tfaSecrets" ADD CONSTRAINT "tfaSecrets_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "backupCodes" ADD CONSTRAINT "backupCodes_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
